<?php
/**
 * Created by PhpStorm.
 * User: anophoo
 * Date: 3/21/18
 * Time: 18:56
 */

main();
echo "<br>";

printColors();
echo "<br>";


function main() {
    $sum = 0;
    for ($i = 0; $i < 10; $i++) {
        $sum += $i;
    }
    echo "sum is $sum<br>";
}

function printColors() {
    $colors = array("red", "green", "blue", "yellow");

    foreach ($colors as $value) {
        echo "$value <br>";
    }
}


function setHeight($minheight = 50) {
    echo "The height is : $minheight <br>";
}

setHeight(350);
setHeight(); // will use the default value of 50
setHeight(135);
setHeight(80);
echo "<br>";


$cars = array("Volvo", "BMW", "Toyota"); // create array in php
echo "I like " . $cars[0] . ", " . $cars[1] . " and " . $cars[2] . ".<br>";
echo "<br>";

$cars = array("Volvo", "BMW", "Toyota");
$arrLength = count($cars);

for($x = 0; $x < $arrLength; $x++) {
    echo $cars[$x];
    echo "<br>";
}
echo "<br>";

$age = array("Peter"=>"35", "Ben"=>"37", "Joe"=>"43");
ksort($age);
echo "Peter is " . $age['Peter'] . " years old.";
echo "<br>";

foreach($age as $x => $x_value) {
    echo "Key = " . $x . ", Value = " . $x_value;
    echo "<br>";
}
echo "<br>";

$cars = array("Volvo", "BMW", "Toyota");
sort($cars);
echo "<br>";

if ($_SERVER["REQUEST_METHOD"] == "POST") {
    echo "post method works";
    echo "<br>";
    // collect value of input field
    $name = $_POST['name'];
    if (empty($name)) {
        echo "Name is empty";
    } else {
        echo $name;
    }
    echo "<br>";
}

